package com.epam;

import java.util.List;

public interface Printer {
    void print(List<Integer> numbers, int action);
}
