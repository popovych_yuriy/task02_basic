package com.epam;

import java.util.List;

public class ConsolePrinter implements Printer {

    @Override
    public void print(List<Integer> numbers, int action) {
        if (action == 1) {
            System.out.println("Odd numbers from start to end:");
            printList(numbers);
        } else if (action == 2) {
            System.out.println("Even numbers from end to start:");
            printList(numbers);
        } else if (action == 3) {
            System.out.println("Sum of odd numbers:");
            System.out.println(numbers.get(0));
            System.out.println("Sum of even numbers:");
            System.out.println(numbers.get(1));
        } else if (action == 4) {
            System.out.println("Fibonacci numbers:");
            System.out.println("The biggest odd number  :" + numbers.get(0));
            System.out.println("The biggest even number  :" + numbers.get(1));
        } else if (action == 5) {
            System.out.println("Fibonacci percentage:");
            System.out.println("The percentage of odd numbers: " + numbers.get(0));
            System.out.println("The percentage of even numbers: " + numbers.get(1));
        } else {
            System.out.println("Printer invalid.");
        }
    }

    private void printList(List<Integer> numbers) {
        for (Integer number : numbers) {
            System.out.println(number);
        }
    }
}
