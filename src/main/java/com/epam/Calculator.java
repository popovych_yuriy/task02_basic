package com.epam;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.reverse;

public class Calculator {
    public List<Integer> calculate(int start, int last, int action) {
        List<Integer> result = new ArrayList<>();

        if (action == 1) {
            result = calculateOdd(start, last);
        } else if (action == 2) {
            List<Integer> even = calculateEven(start, last);
            reverse(even);
            result = even;
        } else if (action == 3) {
            result.add(calculateSumOdd(start, last));
            result.add(calculateSumEven(start, last));
        } else if (action == 4) {
            result = calculateFibonacci(start);
        } else {
            result = calculateFibonacciPercentage(start);
        }
        return result;
    }

    private List<Integer> calculateOdd(int start, int last) {
        List<Integer> result = new ArrayList<>();
        while (start < last) {
            if (start % 2 != 0) {
                result.add(start);
            }
            start++;
        }
        return result;
    }

    private List<Integer> calculateEven(int start, int last) {
        List<Integer> result = new ArrayList<>();
        while (start < last) {
            if (start % 2 == 0) {
                result.add(start);
            }
            start++;
        }
        return result;
    }

    private int calculateSumOdd(int start, int last) {
        List<Integer> oddNumbers = calculateOdd(start, last);
        int sum = 0;
        for (int i = 0; i < oddNumbers.size(); i++) {
            sum += oddNumbers.get(i);
        }
        return sum;
    }

    private int calculateSumEven(int start, int last) {
        List<Integer> evenNumbers = calculateEven(start, last);
        int sum = 0;
        for (int i = 0; i < evenNumbers.size(); i++) {
            sum += evenNumbers.get(i);
        }
        return sum;
    }

    private List<Integer> calculateFibonacci(int size) {
        List<Integer> result = new ArrayList<>();
        int previous = 0;
        int next = 1;
        int sum;
        int maxOdd = 0;
        int maxEven = 0;

        for (int i = 2; i <= size; i++) {
            sum = previous;
            previous = next;
            next = sum + previous;

            if (next % 2 != 0) {
                maxOdd = next;
            } else {
                maxEven = next;
            }
        }
        result.add(maxOdd);
        result.add(maxEven);
        return result;
    }

    private List<Integer> calculateFibonacciPercentage(int size) {
        List<Integer> result = new ArrayList<>();
        int previous = 0;
        int next = 1;
        int sum;
        float amountOdd = 0.0f;
        float amountEven = 0.0f;
        float percentageOdd;
        float percentageEven;

        for (int i = 2; i <= size; i++) {
            sum = previous;
            previous = next;
            next = sum + previous;

            if (next % 2 != 0) {
                amountOdd++;
            } else {
                amountEven++;
            }
        }
        percentageOdd = (amountOdd / (amountOdd + amountEven)) * 100.0f;
        percentageEven = (amountEven / (amountOdd + amountEven)) * 100.0f;

        result.add((int) percentageOdd);
        result.add((int) percentageEven);

        return result;
    }
}
