package com.epam;

import java.util.List;
import java.util.Scanner;

/**
 * <H1>Task 2 Basic</H1>
 * Write program (Maven project), which will pass requirements:
 * - User enter the interval (for example: [1;100]);
 * - Program prints odd numbers from start to the end of interval and even from end to start;
 * Program prints the sum of odd and even numbers;
 * Program build Fibonacci numbers: F1 will be the biggest odd number and F2 – the biggest even number,
 * user can enter the size of set (N);
 * Program prints percentage of odd and even Fibonacci numbers;
 * Create and generate JavaDoc.
 * Add the following plugins to Maven project: FindBugs, Checkstyle, PMD. Then execute the command “mvn site”
 * and view the results of its execution in the folder “target/site”.
 * Fix all errors in the code that plugins will detect.
 */
public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Choose action: \n" +
                    "1 - Print odd numbers from start to end\n" +
                    "2 - Print even numbers from end to start\n" +
                    "3 - Print sum of odd and even numbers\n" +
                    "4 - Print Fibonacci numbers:\n" +
                    "    F1 will be the biggest odd number and\n" +
                    "    F2 the biggest even number\n" +
                    "5 - Print percentage of odd and even Fibonacci numbers");
            int action = scanner.nextInt();

            Calculator calculator = new Calculator();
            List<Integer> result = null;

            if ((action == 4) || (action == 5)) {
                System.out.println("Please write the size of set:");
                int size = scanner.nextInt();
                result = calculator.calculate(size, 0, action);
            } else {
                System.out.println("Please write start number:");
                int start = scanner.nextInt();
                System.out.println("Please write last number:");
                int last = scanner.nextInt();
                result = calculator.calculate(start, last, action);
            }

            ConsolePrinter printer = new ConsolePrinter();
            printer.print(result, action);
        } catch (Exception e) {
            System.out.println("Invalid input");
        }
    }
}
